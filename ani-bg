#!/usr/bin/env bash

EXE_NAME="${0##*/}"
PIDFILE="/var/run/user/$UID/${EXE_NAME}.pid"
PID_P=$(cat "${PIDFILE}")
CONFIG_DIR="${HOME}/.config/${EXE_NAME}"
CONFIG_FILE="${CONFIG_DIR}/${EXE_NAME}.conf"
BG_DIR="${CONFIG_DIR}/backgrounds"
XWW=$(which xwinwrap)

ps -ax | awk '{print $1,$6;}' | grep "$0$" | grep -v "$$" && exit 10

mkdir -p "${CONFIG_DIR}"
grep "BG.*=" "${CONFIG_FILE}" >/dev/null || echo "BG_P=" > "${CONFIG_FILE}"
source "${CONFIG_FILE}"
test ! -f "${BG_DIR}/${BG_P}" && BG_P=

action=$(test -n "$1" && echo "-$1 1")
is_ps=$(test -n "${PID_P}" && (ps -ax | grep "^ *${PID_P}"))

if [ "${is_ps}" ] && [ ! "${action}" ]; then
  kill "${PID_P}"
  test ! "${action}" && echo "" > "${PIDFILE}" && exit 0
fi

bg_n=$(ls -1 "${BG_DIR}" | grep -x "${action}" "${BG_P}" | grep -v "${BG_P}" || echo "${BG_P}")
bg_n=${bg_n:-$(ls -1 "${BG_DIR}" | head -n1)}

sed -i "s/\(BG_P *= *\).*/\1${bg_n}/" "${CONFIG_FILE}"

geometry=$(xrandr -q | grep ' connected' | grep -oP '\d+x\d+\+\d+\+\d+')
exec "${XWW}" -ov -ni -fs -g "${geometry}" --\
  mpv --no-stop-screensaver \
  --vo=vdpau --hwdec=vdpau \
  --loop-file --no-audio \
  --no-osc --no-osd-bar \
  --no-input-default-bindings -wid WID \
  "${BG_DIR}/${bg_n}" &

echo $! > "$PIDFILE"

sleep 0.1
test "${action}" && kill "${PID_P}"
sleep 0.1
